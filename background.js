// foolscreen background page
//
// TODO
//    remove windows from coord array on window delete

console.log("===background START===");

var dockTopCoords = new Array ,
    dockLeftCoords = new Array;

//desc: Message handler for messages from content scripts and injected pages
chrome.runtime.onMessage.addListener(function(msg, _, sendResponse) {
  if (msg.testMessage) {
    sendResponse({bla: "BG page received test message: ",  message: msg.message});
    return true;
  } else if (msg.storeDockCoords) {
    chrome.windows.getCurrent( function(window) {
      dockTopCoords[window.id] = msg.dockTopCoord;
      dockLeftCoords[window.id] = msg.dockLeftCoord;
    });
    return true;
  } else if (msg.activateTab) {
    chrome.tabs.update(msg.tabId, {active: true});
    return true;
  } else if (msg.getIconUrl) {
    sendResponse( {url: chrome.runtime.getURL('icon.png')} );
    return true;
    /// QQQ
  } else if (msg.gotoUrl) {
    chrome.tabs.query( {active: true, currentWindow: true} , function(tab) {
      console.log('received gotoUrl message ', tab );
      url=msg.url;
      if ( url.indexOf("http://") != 0  && url.indexOf("https://") != 0) {
        url="http://" + url;
      }
      chrome.tabs.update(tab.id, {url: url});
    });
    return true;
    /// QQQ
  }
  // If we don't return anything, the message channel will close, regardless
  // of whether we called sendResponse.
});

/*
// determine current window
// for each tab in window
//   sendmesagge updateDockCoords
  chrome.windows.get( tab.windowId, {populate: true} , function(window) {
    chrome.tabs.sendMessage( tab.id, {updateDock: true , windowId: window.id , tabs: window.tabs, dockTopCoord: dockTopCoords[window.id] , dockLeftCoord: dockLeftCoords[window.id]} );
*/

console.log("===background 1===");

//=== Install dockbar updater
// Sends a message to update the dockbar, along with the list of all tabs in the window of the tab
chrome.tabs.onActivated.addListener(function(tabId) { sendDockUpdateMsg(); console.log('bg: activated: '+tabId);});
chrome.tabs.onUpdated.addListener(function(tabId)   { sendDockUpdateMsg(); console.log('bg: updated:   '+tabId);});
chrome.tabs.onRemoved.addListener(function(tabId)   { sendDockUpdateMsg(); console.log('bg: removed:   '+tabId);});
chrome.tabs.onMoved.addListener(function(tabId)     { sendDockUpdateMsg(); console.log('bg: moved:     '+tabId);});
chrome.tabs.onAttached.addListener(function(tabId)  { sendDockUpdateMsg(); console.log('bg: attached:  '+tabId);});

// desc: Call the updateTab function for all active tabs
// in:   tabId    Id of tab that triggered the calling of this function.
function sendDockUpdateMsg() {
  chrome.tabs.query( {active: true} , function(tabs) {
    for (var i = 0; i < tabs.length; i++) {
      updateTab( tabs[i] );
    }
  });
}

// desc:  Send message to content script (CS) and dock script (DS) to update the dock bar.
//        CS generates the tabs and address bar. DS handles the setting of the dock position on screen. 
// note:  This function is not inlined because calling the callback from a loop would fail due to how closure works.
// in:    tab: Tab object that requires updating.
// out:   -
function updateTab(tab) {
/*XXX  console.log('bg: Updatetab called. windowid : ' + tab.windowId + ' tabid: ' + tab.id); */
  chrome.windows.get( tab.windowId, {populate: true} , function(window) {
    chrome.tabs.sendMessage( tab.id, {updateDock: true , windowId: window.id , tabs: window.tabs, dockTopCoord: dockTopCoords[window.id] , dockLeftCoord: dockLeftCoords[window.id]} );
  });
}

/*
chrome.management.onDisabled.addListener( function(extensionInfo) {
  console.log('Extension disabled: ' + id);
  //TODO: Send message to contentscripts to hide the iframe
});

chrome.management.onEnabled.addListener( function(extensionInfo) {
  console.log('Extension enabled: ' + id);
  //TODO: Send message to contentscripts to show the iframe
});
*/

console.log("===background END===");

//==============================================

/*
// Experiment: Cat detector
console.log("===PiriPiri 11===");
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo) {
  if (changeInfo.status === 'complete') {
    chrome.tabs.executeScript(tabId, {
      code: ' if (document.body.innerText.indexOf("Cat") !=-1) {' +
            '     alert("Cat not found!");' +
      ' }'
    });
  }
});
*/
