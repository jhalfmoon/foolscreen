// This code is run inside the iframe
//
// It's main purpose is to modify the DOM and add even listeners within the iframe because
// the context script and background page are not authorized to access iframes do that.
//
// NOTES:
// - The iframe can only send messages directly the content script and itself, using parent.postMessage
// - Messages meant for the background script must be relayed through the content script
// - The iframe can receive messages directly from either background script and content script

console.log("===Dock START===");

// desc:  Dock update listener; Update dock if background page requests it.
// in:    tab: Array of Tabs of the window containing this tab (see chrome extensin Tab api)
// out:   -
chrome.runtime.onMessage.addListener(function(msg, _, sendResponse) {
  if (msg.updateDock) {
    updateDock(msg.tabs);
    return true;
  }
  // If we don't return anything, the message channel will close, regardless
  // of whether we called sendResponse.
});

// desc:  Keyboard input handler for dock address input
document.getElementById("dockAddr").addEventListener("keyup", function(event) {
  console.log(event.currentTarget.id + ' ' + event.keyCode );
  if (event.keyCode == 13) {
    console.log('enter pressed!');
    window.parent.postMessage({ signalUrlChange: true , url: event.currentTarget.value }, "*");
  }
}, false);

// QQQ
/*
// desc:  Test button listener; Send message to bg page that button has been clicked.
// in:    -
// out:   -

document.getElementById("dockTestButton").addEventListener("click", function(event) {
  chrome.runtime.sendMessage( {testMessage2: true, message: "Test: Dock.js says hi!"}, function(response) {
    console.log('dock: ' + response.message);
  });
}, false);
*/
// QQQ

// desc:  Turn the array of tabs into a nice list and add identifiers to the list items.
// int:   tabs:Array of tab indexed by tab.id
// out:   -
function updateDock(tabs) {
  // first get the location of the default favicon
  chrome.runtime.sendMessage( {getIconUrl: true} , function(response) {
    defaultFavicon = response.url;
    // now update the dock
    var newNavList = document.createElement("ul");
    newNavList.id = "dockNavList";
    for (var i = 0; i < tabs.length; i++) {
      index = tabs[i].index;
      // use title as identifier, if available
      if (tabs[i].title.length) {
        name = tabs[i].title;
      } else {
        name = tabs[i].url;
      }
      // prevent any html in the title from screwing up the presentation
      name = name.replace(/<|>/g, " ")
      var text = document.createElement("p");
      text.innerHTML = name;
  
      var img = document.createElement("img");
      img.className = 'favIcon';
      iconUrl = tabs[i].favIconUrl;
      // If favicon url is empty or points to a chrome-internal location, then give it a default icon
      if (! iconUrl.length === undefined || iconUrl.indexOf("IDR_EXTENSIONS_FAVICON") !== -1 ) {
        iconUrl = defaultFavicon;
      }
      img.src = iconUrl;
  
      var anchor = document.createElement("a");
      anchor.href = "#";
      anchor.id = tabs[i].id;
      anchor.appendChild(img);
      anchor.appendChild(text);
      if (tabs[i].active) {
        anchor.className  = "activeTab";
        var adr = tabs[i].url;
      };
      anchor.addEventListener("click", function() { tabClicked(event);} );
  
      var item = document.createElement("li");
      item.appendChild(anchor);
      newNavList.appendChild(item);
    }
    oldNavList = document.getElementById('dockNavList');
    oldNavList.parentNode.replaceChild(newNavList, oldNavList);
    document.getElementById("dockAddr").value = adr;
    setIframeDimensions();
  });
}

// desc:  Tab click listener; Send message to background page to activate the selected tab 
// in:    event: event object 
// out:   -
function tabClicked(event) {
  chrome.runtime.sendMessage( {activateTab: true , tabId: parseInt(event.currentTarget.id)} );
}

// desc:  Calculate dimension of iframeInnerContainer and send it to the content
//        script to adjust the actual dimension of the iframe.
function setIframeDimensions() {
  // get calculated margin size of iframe body
  bodyStyle = window.getComputedStyle(document.body);
  xMargin = parseInt(bodyStyle.marginLeft) + parseInt(bodyStyle.marginRight);
  yMargin = parseInt(bodyStyle.marginTop) + parseInt(bodyStyle.marginBottom);
  // calculate total size by adding container div dimensions
  element = document.getElementById('dockIframeInnerContainer');
  var width  = element.scrollWidth + xMargin;
  var height = element.scrollHeight + yMargin;
  window.parent.postMessage({ setIframeDimensions : true , width: width , height: height }, "*");
}

console.log("===Dock END===");
