// foolscreen new tab replacement

/*
window.onload = function() {
  document.getElementById("newtabAddr").focus();
}

// add to manifest:
  "chrome_url_overrides" : {
    "newtab": "newtab.html"
  },
*/

//document.addEventListener('load', function() {
window.addEventListener('load', function() {
  document.getElementById("newtabAddr").focus();
}, false);

// desc:  Keyboard input handler for dock address input
document.getElementById("newtabAddr").addEventListener("keyup", function(event) {
  console.log(event.currentTarget.id + ' ' + event.keyCode );
  if (event.keyCode == 13) {
    console.log('enter pressed!');
    chrome.tabs.getCurrent( function(tab) {
      chrome.tabs.update(tab.id, {url: event.target.value});
    });
  }
}, false);

   
