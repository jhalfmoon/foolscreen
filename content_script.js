// foolscreen content script
// injects the dock iframe and iframe wrapper

console.log("===Content script START===");

// desc:  Message handler for requests from dock iframe
window.addEventListener("message", function(event) {
  iframe = document.getElementById('dockIframe');
  // Only accept messages from 'our' iframe
  if (event.source != iframe.contentWindow) {
    return;
  }
  // If iframe requests a resize of its dimensions, then do so
  if ( event.data.setIframeDimensions) {
    iframe.height = event.data.height;
    iframe.width = event.data.width;
    return;
  // If iframe signals an URL change, then pass the message to the BG script to reload the tab with the new URL
  } else if ( event.data.signalUrlChange) {
    console.log('Received signalUrlChange message.');
    chrome.runtime.sendMessage( {gotoUrl : true , url : event.data.url } );
    return true;
  }
}, false);

// desc:  Message handler for requests from background tab
chrome.runtime.onMessage.addListener(function(msg, _, sendResponse) {
// injectFrame: Used only for testing thus far.
  if (msg.injectIframe) {
    injectIframe();
    return(true);
// If background page signals activity on the iframe, then update the coords of the iframe
  } else if (msg.updateDock || msg.updateDockCoords) {
    iframeContainer = document.getElementById('dockIframeContainer');
    iframeContainer.style.top = msg.dockTopCoord;
    iframeContainer.style.left = msg.dockLeftCoord;  
    return(true);
  } 
  // If we don't return anything, the message channel will close, regardless
  // of whether we called sendResponse.
});

//QQQ
// Send a test message
chrome.runtime.sendMessage( {testMessage: true, message: "Test: content_script.js says hi!"} , function(response){
  console.log('inj: ' + response.message);
});
//QQQ

document.addEventListener('DOMContentLoaded', injectIframe, false);

console.log("===Content script END===");

//=== Init ====================================================================
// desc:  Inject an iframe into the current page and initialize it. This iframe will contain the dock where all the magic happens
function injectIframe() {
  console.log("===Content scripts 1===");
  //QQQ
  // Add debug text to the beginning of the documen
  debugText=document.createElement('p');
  debugText.innerHTML='===foolscreen===';
  document.body.insertBefore(debugText, document.body.firstChild);
  //QQQ
  
  // Inject floating iframe in page, ie. the actual dock bar
  var dockIframe = document.createElement("iframe");
  dockIframe.id = "dockIframe";
  dockIframe.src = chrome.extension.getURL("dock.html");
  var dockIframeContainer = document.createElement("div");
  dockIframeContainer.id = "dockIframeContainer";
  dockIframeContainer.appendChild(dockIframe);
  document.body.insertBefore(dockIframeContainer , document.body.firstElementChild);

  // desc:  Send dockIframeContainer coords to bg page when mouse click activity is detected. 
  //        This is used to sync the positions of the docks on all pages.
  dockIframeContainer.addEventListener( "mouseup" , function(event) {
      chrome.runtime.sendMessage( {storeDockCoords : true , windowId: window.id ,dockTopCoord: dockIframeContainer.style.top , dockLeftCoord: dockIframeContainer.style.left } );
  });

  // Make iframe draggable (using jquery-ui)
  $(function () {
    $("#dockIframeContainer")
      .resizable()
      .draggable();
  // Note: Position must be forced to 'fixed'. If not done, then on some
  // pages it will fall back to 'relative', which breaks the iframe positioning.
    $("#dockIframeContainer").css("position","fixed");
  });
  
  // Install alpha fading of dock on mouse-over (using jquery-ui)
  $('#dockIframeContainer').hover(
    function() {
  //    $(this).stop().fadeTo('fast', 1);
      $(this).stop().fadeTo('fast', 0.9);
    },
    function(){
  //    $(this).stop().fadeTo('fast', 1);
      $(this).stop().fadeTo('fast', 0.3);
    }
  );
}
